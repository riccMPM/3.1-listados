package com.pe.listadoLis

data class Meme(
        var nombre: String,
        var imagen: Int,
        var url: String
)