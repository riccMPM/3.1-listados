package com.pe.listadoLis

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MemeAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /* List<String> lstMeme = new ArrayList<>();
        lstMeme.add("Meme1");
        lstMeme.add("Meme2");
        lstMeme.add("Meme3");
        lstMeme.add("Meme4");*/

        //Creamos Objtos
        val meme1 = Meme("YaoMing", R.drawable.m1, "")
        val meme2 = Meme("Fua", R.drawable.m2, "")
        val meme3 = Meme("Serious", R.drawable.m3, "")
        val meme4 = Meme("No", R.drawable.m4, "")


        //Los llevamos al listado
        val lstMeme: MutableList<Meme> = ArrayList()
        lstMeme.add(meme1)
        lstMeme.add(meme2)
        lstMeme.add(meme3)
        lstMeme.add(meme4)
        adapter = MemeAdapter(lstMeme)

        //Llenar informacion listado
        rviListado.setHasFixedSize(true)
        rviListado.layoutManager = LinearLayoutManager(this)
        rviListado.adapter = adapter
    }
}