package com.pe.listadoLis

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pe.listadoLis.util.inflate
import kotlinx.android.synthetic.main.item_meme.view.*

class MemeAdapter(private var arrayMeme: MutableList<Meme>) : RecyclerView.Adapter<MemeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //Este metodo hace la relacion con el layout del item

        val inflatedView = parent.inflate(R.layout.item_meme, false)

        return ViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //Este metodo itera de acuerdo a lo que indicas en el metodo getItemCount

        val meme = arrayMeme[position]
        holder.bindProduct(meme)

    }

    override fun getItemCount(): Int {
        //Aqui indicas cuantas filas tendra tu RecyclerView
        return arrayMeme.size
    }


    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bindProduct(meme: Meme) {
            view.tviMeme.text = meme.nombre
            view.iviMeme.setImageDrawable(view.resources.getDrawable(meme.imagen))
        }

    }

}